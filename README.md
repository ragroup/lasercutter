
# README Laser Cutter

## Quick start
  1.   
    * Turn machine on and use the arrow keys on the machine to move the cutting head to the back right corner
    * Decide if you should use the knife bed or honnycomb bed 
    * REMEMBER TO ADJUST THE Z-HEIGHT TO THE CHOSEN CUTTING BED SO YOU DON'T RAM THE CUTTING HEAD AGAINST THE BED!
    * Put in your material (use only MDF, acrylic or cardboard for beginners) 
    * Move the cutting head to somewhere smooth an even on your material
    * Use the autofocus feature to set the z-height.
  1.  
    * Use LightBurn software to handle your cut file. 
    * choose cut/engrave settings for each layer in LightBurn 
    * Press the send button in lightburn to send the file to the cutter
  1.  
    * press the file button the the lasercutter and choose your file (usually the first on the list) 
    * Use the arrow keys on the lasercutter to move the cutting head to where you want to cut 
    * press the frame button to see where the laser intends to cut 
    * Close the lid 
    * start ventilation, pump and laserpower
    * Press start on the lasercutter
  1.  
    * DO NOT LEAVE THE LASERCUTTER UNATTENDED. 
    STAY AND KEEP AN EYE THAT EVERYTHING IS OK THROUGHOUT THE WHOLE CUT.
    * DO NOT OPEN THE LID IMMEDIATELY AFTER THE CUT HAS FINNISHED.
    WAIT AT LEAST 15 SECONDS FOR THE FUMES TO BE SUCKED OUT
  1.  
    * Turn off laserpower, ventilation and pump and then finally the machine.
    
## Material safety
The CO2 laser in the machine can cut most organic materials and plastics, but there are some dangerous ones. If you are unsure if something can be cut, ask first.

Safe to cut:

  * Woods 
  * Cardboard/ paper
  * Acrylic

DO NOT CUT any of these:

* PVC, Vinyl
* ABS
* HDPE
* Fiberglass, PCBs
* Carbon fiber


## Material cutting parameters guidelines
Material |Cut params 
---------|----------
Cardboard 2 layer | 120 mm/s  92% 
Cardboard 3 layer | 80 mm/s  92% 
---|---|---
MDF 3 mm | 18 mm/s  92% 
MDF 6 mm | 12 mm/s  92%	
MDF 10 mm | 6 mm/s  92% 
---|---|---
Plywood 6 mm | 5 mm/s -- max 92%, min 92% -- Use high air flow 4bar pressure. -- z-offset 3 mm in -- pwm 0.2kHz -- start end pause time 200 ms cut throug = true
Plywood 8 mm | 5 mm/s max 100%, min 100% -- Use high air flow approx. 5-6 bar pressure -- z-offset 0mm, z-step 2.0 mm in for each pass 
---|---|---
Acrylic 3 mm | 20 mm/s 100% 
Acryllic 12 mm | 3 mm/s 100%
---|---|---
PVC	| ABSOLUTLY NEVER EVER CUT PVC. IT PRODUCES TOXIC AND CORROSIVE GASSES THAT IS BAD FOR YOU AND THE MACHINE.
---|---|---
Polycarbonate | Can be cut simmelary to acrylic, but produces bad smelling and toxic gasses. Burns really easily and your cuts will get brown melty edges. Be causious if you have to cut. Found better cut parameters? please share them here.

## Material engraving parameters guidelines
Material | Engrave params
---------|----------
Cardboard 2 layer | 520mm/s 25%   -6mm z-offset
---|---|---
MDF | 300mm/s 30% -4mm Z-offset, 0.1mm line interval
MDF Vector engrave | 20 mm/s 13% power Z-offset = 2mm in
---|---|---
Acrylic Deep engrave | 200 mm/s 30% -1 mm Z-offset, 0.1 mm line interval
Acrylic Fine. Good for images| 500 mm/s 20% 600 dpi/0.041 mm line interval
Acrylic Vector engrave | 20 mm/s 13% power Z-offset = 2mm in
---|---|---
PVC	| ABSOLUTLY NEVER EVER CUT OR ENGRAVE PVC. IT PRODUCES TOXIC AND CORROSIVE GASSES THAT IS BAD FOR YOU AND THE MACHINE.
---|---|---
Polycarbonate | Can be engraved simmelary to Acryllic at slightly lower power but results may varry. Palycarbonate melts and burns eisily so be aware. 



## Tuning the parameters
### Cutting
For cutting the basic parameters are speed, power and number of passes.  
In most cases power can be left at 100% and the speed can tweaked so the material is cut all the way through.  
If the material is so thick that it needs a speed below 10 mm/s consider cutting it in 2 passes.  

### Engraving
For engraving there are a number of parameters that affect the outcome:  
Speed and power together define the amount of laser energy that hits a unit of surface.  
Line interval specifies the distance between the individual engraving lines. Reasonable values: 0.05 superfine, 0.1 fine, 0.2 draft  
Z-offset defocuses the laser to make the spot wider. Don't forget that if the spot is wider, more power is required. DO NOT use positive values for Z-offset as the laser head will hit the bed. Recommended values for a given line interval:

Line interval | Z-offset
---|---|---
0.05mm | 0 to -4mm
0.1mm | -2 to -6mm
0.2mm | -6 to -8mm

## FAQ/common fixes
The corners, tips, small details of a cut/engraving are not cut through/weaker

* Try raising the minimum power (common values 10-25%) When the machine is cutting in tight radiuses/corners it has to slow down and that also poportionally reduces the laser power. Raising minimum power can override this.

When I press "frame" on the machine, it moves to a different location before framing the cut.

* Move the machine to where you want to start the cut and press "origin" then try "frame" again.

The PC in the lab can't find the machine

* Make sure the PC is connected to the correct WiFi AP, "Laser cutter"
 
## Laser
9 % seems to be the lowest level, under 9% the laser tube will not fire, it is supposedly rated 120 watts

## Machine
The machine itself is an EDUARD x1250 https://www.eduard.dk/eduart-x1250/ and is based around a Ruida 644XS controller (https://variometrum.hu/pdf/rdc6442g.pdf)

## Software
### Lightburn
https://lightburnsoftware.com/ 
Properly the most complete software for the laser cutter but does require an 80 USD license. It automatically detects the machine when plugged in and accepts all kinds of formats, SVG, DXF, AI, and even bitmaps. Runs on everything, Windows, macs and linux. The university has a license that is installed on the dedicated maker space PC next to the laser cutter. 
 
### Inkscape Plugin
On the cheap, we have https://github.com/jnweiger/inkscape-thunderlaser which worked somewhat out of the box on ubuntu 20.04 with Inkscape. It does have some rough edges, for instance, when clicking "Apply" the plugin starts cutting right away where normally you would expect it to just upload the workpiece to the cutter and then take it from there.
 
### Communication protocol
The machine does not use g-code but instead a proprietary "rd" format. There is not much information about the protocol online, but it is possible to find some reverse engineering projects on Github 
 
